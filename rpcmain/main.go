package main

import (
	"spider-scheduler/rpcserver"
	logger "github.com/alecthomas/log4go"
	"strconv"
	"time"
	"fmt"
	"math/rand"
)

func init() {
	logger.LoadConfiguration("./conf/log4go.xml")
}

func main() {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	for i := 0; i < 100; i++ {
		nn := r.Intn(100)
		if nn < 10 {
			fmt.Print(nn)
			fmt.Print(", ")
		}

	}

	files := make(chan string, 5)
	i := 0
	for i < 1 {
		select {
		case fn := <-files:
			logger.Info(fn)
		case <-time.After(time.Second * 5):
			i++
			logger.Info("success ... ")
			break
		}
	}

	//rpcserver.StartServer()

	size := 10
	go func() {
		for i := 0; i < size; i++ {
			logger.Info("push msg %d ", i)
			rpcserver.PushMsg(rpcserver.GPS, "TEST-" + strconv.Itoa(i))
		}
	}()

	go func() {
		for i := 0; i < size * 2; i++ {
			time.Sleep(1 * time.Second)
			msg := rpcserver.PullMsg(rpcserver.GPS)
			logger.Info("Pull msg %s ", msg)
		}
	}()
	fmt.Println("Game over. ???")
	select {
	}
	fmt.Println("Game over. ")
}
