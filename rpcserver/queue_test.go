package rpcserver

import (
	"testing"
	"strconv"
)

func Test_Execute(t *testing.T) {
	for i:= 0; i<100; i++ {
		t.Logf("push msg %d ",i)
		PushMsg(MOBILE,"TEST-"+strconv.Itoa(i))
	}
}
