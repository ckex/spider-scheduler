package rpcserver

import (
	logger "github.com/alecthomas/log4go"
	"github.com/wendal/errors"
	"time"
	"math/rand"
	"strings"
)

const (
	MOBILE = "mobile"
	GPS = "gps"
)

var r = rand.New(rand.NewSource(time.Now().UnixNano()))

var queueGroup map[string]chan string

func init() {
	queueGroup = make(map[string]chan string, 5)
	queueGroup[MOBILE] = make(chan string, 50)
	queueGroup[GPS] = make(chan string, 5)
}

// 非阻塞
func PullMsg(qid string) (val string) {
	if queue, ok := queueGroup[qid]; ok {
		select {
		case val = <-queue:
		default:
			p := true
			if strings.EqualFold(qid, GPS) {
				p = r.Intn(1000) < 10
			}
			if p {
				logger.Info("%s Empty:%s", qid, val)
			}
		}
	} else {
		logger.Error("Invalid queue(must be %s %s):%s", qid, MOBILE, GPS)
	}
	return
}

// 阻塞
func PushMsg(qid, msg string) error {
	if queue, ok := queueGroup[qid]; ok {
		queue <- msg
	} else {
		logger.Error("Invalid queue(must be %s %s):%s", qid, MOBILE, GPS)
		return errors.New("Invalid queue" + qid)
	}
	return nil
}
