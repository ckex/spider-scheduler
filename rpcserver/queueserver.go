package rpcserver

import (
	"net"
	"gmfs/core/logger"
	"google.golang.org/grpc"
	pb "spider-scheduler/model"
	"spider-scheduler/constant"
	"golang.org/x/net/context"
)

var port string

func init() {
	conf := constant.SpiderConfig
	model := constant.Model
	port = conf.String(model + "::grpc.port")
}

type server struct{}

func (server *server) PullMsg(ctx context.Context, in *pb.QueueRequest) (*pb.QueueResponse, error) {
	return &pb.QueueResponse{
		Msg:PullMsg(in.Name),
	},nil
}

func StartServer() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		logger.Errorf("failed to listen: %v", err)
		panic(err.Error())
	}
	s := grpc.NewServer()
	pb.RegisterQueueServerServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		logger.Errorf("failed to serve: %v", err)
		panic(err.Error())
	}
}


