package util

import (
	"io/ioutil"
	"os"
	"strings"
	"path/filepath"
	"bufio"
	"io"
	logger "github.com/alecthomas/log4go"
	"time"
	"sync"
)

//获取指定目录下的所有文件，不进入下一级目录搜索，可以匹配后缀过滤。
func ListDir(dirPth string, suffix string) (files []string, err error) {
	files = make([]string, 0, 10)
	dir, err := ioutil.ReadDir(dirPth)
	if err != nil {
		return nil, err
	}
	PthSep := string(os.PathSeparator)
	suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	for _, fi := range dir {
		if fi.IsDir() {
			// 忽略目录
			continue
		}
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) {
			//匹配文件
			files = append(files, dirPth + PthSep + fi.Name())
		}
	}
	return files, nil
}

//获取指定目录及所有子目录下的所有文件，可以匹配后缀过滤。
func WalkDir(dirPth, suffix string) (files []string, err error) {
	files = make([]string, 0, 30)
	suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	err = filepath.Walk(dirPth, func(filename string, fi os.FileInfo, err error) error {
		//遍历目录
		//if err != nil { //忽略错误
		// return err
		//}
		if fi.IsDir() {
			// 忽略目录
			return nil
		}
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) {
			files = append(files, filename)
		}
		return nil
	})
	return files, err
}

func ReadlineFile(filepath string, exe func(string)) {
	filenames, err := ListDir(filepath, ".csv")
	if err != nil {
		panic(err.Error())
	}
	var wg sync.WaitGroup
	filesize := len(filenames)
	wg.Add(filesize)
	files := make(chan string, filesize)
	for _, fn := range filenames {
		files <- fn
		//go readfile(wg,fn,exe)
	}

	for i := 0; i < 5; i++ {
		go readfileChan(wg, files, exe)
	}
	wg.Wait()
}

func readfileChan(wg sync.WaitGroup, files chan string, exe func(string)) {
	i := 0
	for i < 1 {
		select {
		case fn := <-files:
			readfile(wg, fn, exe)
		case <-time.After(time.Minute):
			i++
			logger.Info("success ... ")
			break
		}
	}
}
func readfile(wg sync.WaitGroup, fn string, exe func(string)) {
	defer wg.Done()
	f, err := os.Open(fn)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	rd := bufio.NewReader(f)
	st := time.Now().Unix()
	logger.Info("处理文件 %s,%d", fn, st)
	linenum := 0
	for {
		line, err := rd.ReadString('\n') //以'\n'为结束符读入一行
		if err != nil || io.EOF == err {
			logger.Info("Over. %s", fn)
			break
		}
		linenum ++
		exe(line)
		if (linenum % 100) == 0 {
			end := time.Now().Unix()
			use := end - st
			logger.Info("%s,处理完%d条,用时 %d", fn, linenum, use)
		}
	}
	end := time.Now().Unix()
	use := end - st
	os.Rename(fn, fn + ".bak")
	logger.Info("处理完成 %s, 用时 %d", fn, use)
}