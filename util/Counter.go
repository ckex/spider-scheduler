package util

import (
	"sync/atomic"
	"time"
	"math/rand"
)

var R *rand.Rand

func init() {
	 R = rand.New(rand.NewSource(time.Now().UnixNano()))
}

type Counter struct {
	v int64
}

func NewCounter(initial int64) *Counter {
	return &Counter{v: initial}
}

func (c *Counter) Val() int64 {
	return atomic.LoadInt64(&c.v)
}


func (c *Counter) Increment() {
	old := atomic.LoadInt64(&c.v)
	swapped := atomic.CompareAndSwapInt64(&c.v, old, old+1)
	for !swapped {
		old = atomic.LoadInt64(&c.v)
		swapped = atomic.CompareAndSwapInt64(&c.v, old, old+1)
	}
}

func (c *Counter) Decrement() {
	old := atomic.LoadInt64(&c.v)
	swapped := atomic.CompareAndSwapInt64(&c.v, old, old-1)
	for !swapped {
		old = atomic.LoadInt64(&c.v)
		swapped = atomic.CompareAndSwapInt64(&c.v, old, old-1)
	}
}

func  GetNum(max int) int  {
	return  R.Intn(max)
}