package umq

import (
	"github.com/ucloud/umq-sdk-go/umq"
	"spider-scheduler/constant"
	logger "github.com/alecthomas/log4go"
	"spider-scheduler/util"
	"time"
	"github.com/astaxie/beego/config"
)

var client *umq.UmqClient

// var queueId string

var producer *umq.UmqProducer
var consumer *umq.UmqConsumer
var count *util.Counter

var sconf config.Configer

var model string

func init() {
	//初始化全局变量
	sconf = constant.SpiderConfig
	model = constant.Model

	publisherId := sconf.String(model + "::umqclient.PublisherId")
	publisherToken := sconf.String(model + "::umqclient.PublisherToken")

	consumerId := sconf.String(model + "::umqclient.ConsumerId")
	consumerToken := sconf.String(model + "::umqclient.ConsumerToken")

	var err error
	client, err = umq.CreateClient(umq.UmqConfig{
		Host:       sconf.String(model + "::umqclient.Host"),
		PublicKey: sconf.String(model + "::umqclient.PublicKey"),
		PrivateKey: sconf.String(model + "::umqclient.PrivateKey"),
		Region:     sconf.String(model + "::umqclient.Region"),
		Account:    sconf.String(model + "::umqclient.Account"),
		ProjectID:  sconf.String(model + "::umqclient.ProjectID"),
	})
	if err != nil {
		panic(err.Error())
	}

	producer = client.NewProducer(publisherId, publisherToken)
	consumer = client.NewConsumer(consumerId, consumerToken)
	count = util.NewCounter(int64(0))
}

func PublishMsg(qId, message string) error {
	st := time.Now().Nanosecond()
	count.Increment()
	err := producer.PublishMsg(qId, message)
	end := time.Now().Nanosecond()
	usd := (end - st) / 1000
	if err != nil {
		logger.Error("[%d]Error,%v (%s)->%s,%d-ms", count.Val(), err, qId, message, usd)
		return err
	}
	if (util.GetNum(10000) < 10) {
		logger.Info("[%d]success(%s)->%s,%d-ms", count.Val(), qId, message, usd)
	}
	return nil
}

func Subscribe() {
	num := util.NewCounter(int64(0))
	qid := sconf.String(model + "::saigegps.umqclient.QueueId")
	logger.Info(" -------- start . ")
	for i := 0; i < 5; i++ {
		//go consumer.SubscribeQueue(qid, func(c chan string, msg umq.Message) {
		//	// 如果需要新起goroutine，需要手动处理
		//	num.Increment()
		//	logger.Info("%d - %s - %s ", num.Val(), msg.MsgId, msg.MsgBody)
		//	consumer.AckMsg(qid, msg.MsgId)
		//
		//})
		go func() {
			for {
				num.Increment()
				msgs, err := consumer.GetMsg(qid, 10)
				if err != nil {
					logger.Error(err.Error())
				}
				logger.Info("result %d, %d",num.Val(), len(msgs.Msgs))
				for _, value := range msgs.Msgs {
					consumer.AckMsg(qid, value.MsgId)
				}

			}

		}()
	}

}


