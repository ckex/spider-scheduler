package constant

import "github.com/astaxie/beego/config"

var  SpiderConfig config.Configer
var Model string

func init() {
	var err error
	SpiderConfig, err = config.NewConfig("ini", "conf/config.ini")
	Model = SpiderConfig.String("model")
	if err != nil {
		panic(err)
	}
}