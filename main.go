package main

import (
	_ "spider-scheduler/constant"
	"spider-scheduler/tasks"
	logger "github.com/alecthomas/log4go"
	"sync"
	"spider-scheduler/constant"
	"github.com/robfig/cron"
	"spider-scheduler/rpcserver"
	"runtime"
)

func init() {
	logger.LoadConfiguration("./conf/log4go.xml")
}

var wg sync.WaitGroup

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	wg.Add(1)
	model := constant.Model
	logger.Info("Start spider scheduler. model=[%s]", model)
	startRpcServer()
	startTask()
	// 消费队列
	//umq.Subscribe()
	wg.Wait()
}

func startRpcServer()  {
	go rpcserver.StartServer()
	logger.Info("start GRPC server successd.")
}

func startTask() {
	scheduler := cron.New()
	tasks.StartSaiGeGPSTask(scheduler)
	tasks.StartJuheMobileTask(scheduler)
	scheduler.Start()
}

