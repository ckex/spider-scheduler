package tasks

import (
	"spider-scheduler/util"
	"spider-scheduler/constant"
	logger "github.com/alecthomas/log4go"
	"spider-scheduler/dao"
	"strings"
	"github.com/robfig/cron"
	//"spider-scheduler/umq"
	"time"
	"spider-scheduler/rpcserver"
)

var juhemobileCronExpression string
var id string
var filepath string
var juheQueueId string

func init() {
	//初始化全局变量
	config := constant.SpiderConfig
	model := constant.Model

	juheQueueId = config.String(model + "::juhemobile.umqclient.QueueId")

	juhemobileCronExpression = config.String(model + "::juhemobile.cron")

	filepath = config.String(model + "::file.path")
	count = util.NewCounter(int64(0))
	id = "1"
}

func StartJuheMobileTask(schedul *cron.Cron) {
	//schedul.AddFunc(juhemobileCronExpression, func() {
	//	mobiles := queueMobile()
	//	for _, value :=  range mobiles {
	//		err := umq.PublishMsg(queueId,value)
	//		if err != nil {
	//			logger.Error("Err %s %v",value,err.Error())
	//		}else{
	//			logger.Info("Execute task. %s",value)
	//		}
	//	}
	//})
	// -----------

	//schedul.AddFunc(juhemobileCronExpression, func() {
	//
	//	logger.Info("Start task . ")
	//	util.ReadlineFile(filepath, func(value string) {
	//		err := umq.PublishMsg(queueId, value)
	//		if err != nil {
	//			logger.Error("Err %s %v", value, err.Error())
	//		} else if (util.GetNum(10000) < 10) {
	//			logger.Info("Execute task. %s", value)
	//		}
	//	})
	//	logger.Info("-------------")
	//})

	// -----------

	ticker := time.NewTicker(time.Second)
	go func() {
		for _ = range ticker.C {
			logger.Info("Start juhe mobile task .")
			util.ReadlineFile(filepath, func(value string) {
				rpcserver.PushMsg(rpcserver.MOBILE,value)
				//umq.PublishMsg(juheQueueId, value)
			})
			logger.Info("Finished juhe mobile task .")
		}
	}()
}

func queueMobile() []string {
	sql := "select id, number from yy_user_address_book where id > next order by id limit 1000;";
	query := strings.Replace(sql, "next", id, 1)
	logger.Info(query)
	//result := dao.ExecuteSimple()
	result := dao.Execute(query)
	mobiles := make([]string, 0)
	for _, value := range result {
		if val, ok := value["number"]; ok {
			if len(val.(string)) > 6 {
				mobiles = append(mobiles, val.(string))
				id = value["id"].(string)
			}
		}
	}
	return mobiles
}
