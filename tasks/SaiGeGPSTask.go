package tasks

import (
	logger "github.com/alecthomas/log4go"
	"spider-scheduler/constant"
	"bytes"
	"strconv"
	"spider-scheduler/util"
	"github.com/robfig/cron"
	"spider-scheduler/rpcserver"
)

var saigegpsCronExpression string
var saigeQueueId string
var count  *util.Counter;

func init() {
	//初始化全局变量
	config := constant.SpiderConfig
	model := constant.Model

	saigeQueueId = config.String(model + "::saigegps.umqclient.QueueId")
	saigegpsCronExpression = config.String(model + "::saigegps.cron")
	count = util.NewCounter(int64(0))
}

func StartSaiGeGPSTask(schedul *cron.Cron) {
	schedul.AddFunc(saigegpsCronExpression, func() {
		count.Increment()
		b := bytes.Buffer{}
		b.WriteString("gps-message-")
		b.WriteString(strconv.FormatInt(count.Val(), 10))
		//err := umq.PublishMsg(saigeQueueId, b.String())
		err := rpcserver.PushMsg(rpcserver.GPS,b.String())
		if err == nil {
			logger.Info("saige gps success [%s]==>%s", saigeQueueId, b.String())
		}
	})
	//ticker := time.NewTicker(time.Second * time.Duration(intervalSecond))
	//go func() {
	//	for _ = range ticker.C {
	//	}
	//}()
}

//func showOrganizationId()  {
//	response ,err := umqclient.GetOrganizationId("bo.zhang@mljr.com","org-paztgm")
//	if err != nil {
//		panic(err)
//	}
//	logger.Info("%+v",response)
//	switch response.(type) {
//	case map[string]interface{}:
//		for key,value := range response.(map[string]interface{}){
//			logger.Info("key =%s type=%s value =%+v",key,reflect.TypeOf(value),value)
//			switch value.(type) {
//			case float64:
//				logger.Info("%s %d",key,int64(value.(float64)))
//			}
//		}
//	}
//}
