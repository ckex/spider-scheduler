package dao


import (
 _ "github.com/go-sql-driver/mysql"
	"database/sql"
)

var db *sql.DB

func init() {
	//initConn()
}

func initConn()  {
	var err error
	db, err = sql.Open("mysql", "dep_risk:0ZAdd7GyiLQkOa8O@tcp(10.8.61.104:3331)/yyuser?charset=utf8")
	if err != nil {
		panic(err.Error())
	}
	db.SetMaxOpenConns(200)
	db.SetMaxIdleConns(100)
	db.Ping()
}

func ExecuteSimple() []map[string]interface{} {
	row := make(map[string]interface{})
	row["id"] = "1"
	row["number"] = "15601662656"

	result := make([]map[string]interface{},1)
	result = append(result,row)
	return result
}

func Execute(queueSql string) []map[string]interface{} {

	// Execute the query
	rows, err := db.Query(queueSql)
	defer rows.Close()
	if err != nil {
		panic(err.Error())
	}


	// Get column names
	columns, err := rows.Columns()
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Make a slice for the values
	values := make([]sql.RawBytes, len(columns))


	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	// references into such a slice
	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	result  := make([]map[string]interface{},1000)
	// Fetch rows
	for rows.Next() {
		// get RawBytes from data
		err = rows.Scan(scanArgs...)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}

		record := make(map[string]interface{})
		// Now do something with the data.
		// Here we just print each column as a string.
		var value string
		for i, col := range values {
			// Here we can check if the value is nil (NULL value)
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
			}
			record[columns[i]] = value
		}
		result = append(result,record)
	}
	if err = rows.Err(); err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	return  result
}


