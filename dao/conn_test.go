package dao

import (
	"testing"
	"fmt"
	"reflect"
	"strings"
)

func Test_Execute(t *testing.T) {
	sql := "select id, number from yy_user_address_book where id > next limit 100";
	query := strings.Replace(sql,"next","1",1)
	result := Execute(query)
	 fmt.Println(reflect.TypeOf(result))
	for index, value := range result {
		if val, ok := value["number"]; ok {
			if len(val.(string)) > 6 {
				fmt.Println("%d,%+v",index,value)
			}
		}
	}
}
